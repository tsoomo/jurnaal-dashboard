import { message } from "antd";
import { useCallback, useEffect, useState } from "react";
import { GetTasksQuery } from "../graphql";
import sdk from "../sdk";

const useTasks = () => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<GetTasksQuery["getTasks"]>();
  const [page, setPage] = useState(0);
  const getTasks = useCallback(() => {
    setLoading(true);
    sdk
      .getTasks({ limit: 30, page: 0, advanced: {} })
      .then((res) => setData(res.getTasks))
      .catch((err) => message.error(err.message))
      .finally(() => setLoading(false));
  }, [page]);

  useEffect(() => {
    getTasks();
  }, [getTasks, page]);

  return {
    loading,
    data,
  };
};

export default useTasks;
