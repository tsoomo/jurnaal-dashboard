import { message } from "antd";
import { useCallback, useEffect, useState } from "react";
import { GetUserQuery } from "../graphql";
import sdk from "../sdk";

const useUserInfo = ({ userId }: { userId: string }) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<GetUserQuery["getUser"]>();

  const getUser = useCallback(() => {
    setLoading(true);
    sdk
      .getUser({ _id: userId })
      .then((res) => setData(res.getUser))
      .catch((err) => message.error(err.message))
      .finally(() => setLoading(false));
  }, [userId]);

  useEffect(() => {
    getUser();
  }, [getUser, userId]);

  return { loading, data };
};

export default useUserInfo;
