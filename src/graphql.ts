import { GraphQLClient } from 'graphql-request';
import * as Dom from 'graphql-request/dist/types.dom';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type AddAttendanceInput = {
  date: Scalars['Float'];
  status: Status;
  user: Scalars['ID'];
};

export type AddSurveyInput = {
  point: Scalars['Int'];
  task: Scalars['ID'];
  user: Scalars['ID'];
};

export type AddTaskInput = {
  createdAt: Scalars['Float'];
  name: Scalars['String'];
  point: Scalars['Int'];
};

export type AddUserInput = {
  password: Scalars['String'];
  role: Role;
  username: Scalars['String'];
};

export type Attendance = {
  __typename?: 'Attendance';
  _id?: Maybe<Scalars['ID']>;
  date: Scalars['Float'];
  status: Status;
  user: User;
};

export type Auth = {
  __typename?: 'Auth';
  token: Scalars['String'];
  user: User;
};

export type DateRange = {
  endDate?: InputMaybe<Scalars['Float']>;
  startDate?: InputMaybe<Scalars['Float']>;
};

export type EditAttendanceInput = {
  date?: InputMaybe<Scalars['Float']>;
  status?: InputMaybe<Status>;
  user?: InputMaybe<Scalars['ID']>;
};

export type EditSurveyInput = {
  point?: InputMaybe<Scalars['Int']>;
  task?: InputMaybe<Scalars['ID']>;
  user?: InputMaybe<Scalars['ID']>;
};

export type EditTaskInput = {
  createdAt?: InputMaybe<Scalars['Float']>;
  name?: InputMaybe<Scalars['String']>;
  point?: InputMaybe<Scalars['Int']>;
};

export type EditUserInput = {
  password?: InputMaybe<Scalars['String']>;
  role?: InputMaybe<Role>;
  username?: InputMaybe<Scalars['String']>;
};

export type GetAttendancesResult = {
  __typename?: 'GetAttendancesResult';
  attendances?: Maybe<Array<Attendance>>;
  totalPages: Scalars['Int'];
};

export type GetSurveysResult = {
  __typename?: 'GetSurveysResult';
  surveys?: Maybe<Array<Survey>>;
  totalPages: Scalars['Int'];
};

export type GetTasks = {
  date?: InputMaybe<DateRange>;
};

export type GetTasksResult = {
  __typename?: 'GetTasksResult';
  tasks?: Maybe<Array<Task>>;
  totalPages: Scalars['Int'];
};

export type GetUsersResult = {
  __typename?: 'GetUsersResult';
  totalItems: Scalars['Int'];
  totalPages: Scalars['Int'];
  users?: Maybe<Array<User>>;
};

export type Mutation = {
  __typename?: 'Mutation';
  addAttendance: Scalars['Boolean'];
  addSurvey: Scalars['Boolean'];
  addTask: Scalars['Boolean'];
  addUser: Scalars['Boolean'];
  deleteAttendance: Scalars['Boolean'];
  deleteSurvey: Scalars['Boolean'];
  deleteTask: Scalars['Boolean'];
  deleteUser: Scalars['Boolean'];
  editAttendance: Scalars['Boolean'];
  editSurvey: Scalars['Boolean'];
  editTask: Scalars['Boolean'];
  editUser: Scalars['Boolean'];
  login: Auth;
  register: Scalars['Boolean'];
  version: Scalars['String'];
};


export type MutationAddAttendanceArgs = {
  attendance: AddAttendanceInput;
};


export type MutationAddSurveyArgs = {
  survey: AddSurveyInput;
};


export type MutationAddTaskArgs = {
  task: AddTaskInput;
};


export type MutationAddUserArgs = {
  user: AddUserInput;
};


export type MutationDeleteAttendanceArgs = {
  _id: Scalars['ID'];
};


export type MutationDeleteSurveyArgs = {
  _id: Scalars['ID'];
};


export type MutationDeleteTaskArgs = {
  _id: Scalars['ID'];
};


export type MutationDeleteUserArgs = {
  _id: Scalars['ID'];
};


export type MutationEditAttendanceArgs = {
  _id: Scalars['ID'];
  attendance: EditAttendanceInput;
};


export type MutationEditSurveyArgs = {
  _id: Scalars['ID'];
  survey: EditSurveyInput;
};


export type MutationEditTaskArgs = {
  _id: Scalars['ID'];
  task: EditTaskInput;
};


export type MutationEditUserArgs = {
  _id: Scalars['ID'];
  user: EditUserInput;
};


export type MutationLoginArgs = {
  password: Scalars['String'];
  username: Scalars['String'];
};


export type MutationRegisterArgs = {
  user: AddUserInput;
};

export type Query = {
  __typename?: 'Query';
  getAttendances: GetAttendancesResult;
  getSurvey: Survey;
  getSurveys: GetSurveysResult;
  getTask: Task;
  getTasks: GetTasksResult;
  getUser: User;
  getUsers: GetUsersResult;
  version: Scalars['String'];
};


export type QueryGetAttendancesArgs = {
  limit: Scalars['Int'];
  page: Scalars['Int'];
  user: Scalars['ID'];
};


export type QueryGetSurveyArgs = {
  _id: Scalars['ID'];
};


export type QueryGetSurveysArgs = {
  limit: Scalars['Int'];
  page: Scalars['Int'];
  user: Scalars['ID'];
};


export type QueryGetTaskArgs = {
  _id: Scalars['ID'];
};


export type QueryGetTasksArgs = {
  advanced: GetTasks;
  limit: Scalars['Int'];
  page: Scalars['Int'];
};


export type QueryGetUserArgs = {
  _id: Scalars['ID'];
};


export type QueryGetUsersArgs = {
  limit: Scalars['Int'];
  page: Scalars['Int'];
};

export enum Role {
  Student = 'STUDENT',
  Teacher = 'TEACHER'
}

export enum Status {
  Absent = 'ABSENT',
  Free = 'FREE',
  Present = 'PRESENT',
  Sick = 'SICK'
}

export type Survey = {
  __typename?: 'Survey';
  _id?: Maybe<Scalars['ID']>;
  point: Scalars['Int'];
  task: Task;
  user: User;
};

export type Task = {
  __typename?: 'Task';
  _id?: Maybe<Scalars['ID']>;
  createdAt: Scalars['Float'];
  name: Scalars['String'];
  point: Scalars['Int'];
};

export type User = {
  __typename?: 'User';
  _id?: Maybe<Scalars['ID']>;
  password: Scalars['String'];
  role: Role;
  username: Scalars['String'];
};

export type DeleteAttendanceMutationVariables = Exact<{
  _id: Scalars['ID'];
}>;


export type DeleteAttendanceMutation = { __typename?: 'Mutation', deleteAttendance: boolean };

export type EditAttendanceMutationVariables = Exact<{
  _id: Scalars['ID'];
  attendance: EditAttendanceInput;
}>;


export type EditAttendanceMutation = { __typename?: 'Mutation', editAttendance: boolean };

export type AddAttendanceMutationVariables = Exact<{
  attendance: AddAttendanceInput;
}>;


export type AddAttendanceMutation = { __typename?: 'Mutation', addAttendance: boolean };

export type GetAttendancesQueryVariables = Exact<{
  page: Scalars['Int'];
  limit: Scalars['Int'];
  user: Scalars['ID'];
}>;


export type GetAttendancesQuery = { __typename?: 'Query', getAttendances: { __typename?: 'GetAttendancesResult', totalPages: number, attendances?: Array<{ __typename?: 'Attendance', _id?: string | null, status: Status, date: number, user: { __typename?: 'User', _id?: string | null, username: string } }> | null } };

export type GetTaskQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GetTaskQuery = { __typename?: 'Query', getTask: { __typename?: 'Task', _id?: string | null, name: string, point: number, createdAt: number } };

export type DeleteSurveyMutationVariables = Exact<{
  _id: Scalars['ID'];
}>;


export type DeleteSurveyMutation = { __typename?: 'Mutation', deleteSurvey: boolean };

export type EditSurveyMutationVariables = Exact<{
  _id: Scalars['ID'];
  survey: EditSurveyInput;
}>;


export type EditSurveyMutation = { __typename?: 'Mutation', editSurvey: boolean };

export type AddSurveyMutationVariables = Exact<{
  survey: AddSurveyInput;
}>;


export type AddSurveyMutation = { __typename?: 'Mutation', addSurvey: boolean };

export type GetSurveysQueryVariables = Exact<{
  page: Scalars['Int'];
  limit: Scalars['Int'];
  user: Scalars['ID'];
}>;


export type GetSurveysQuery = { __typename?: 'Query', getSurveys: { __typename?: 'GetSurveysResult', totalPages: number, surveys?: Array<{ __typename?: 'Survey', _id?: string | null, point: number, user: { __typename?: 'User', _id?: string | null, username: string }, task: { __typename?: 'Task', _id?: string | null, point: number, name: string, createdAt: number } }> | null } };

export type DeleteTaskMutationVariables = Exact<{
  _id: Scalars['ID'];
}>;


export type DeleteTaskMutation = { __typename?: 'Mutation', deleteTask: boolean };

export type EditTaskMutationVariables = Exact<{
  _id: Scalars['ID'];
  task: EditTaskInput;
}>;


export type EditTaskMutation = { __typename?: 'Mutation', editTask: boolean };

export type AddTaskMutationVariables = Exact<{
  task: AddTaskInput;
}>;


export type AddTaskMutation = { __typename?: 'Mutation', addTask: boolean };

export type GetTasksQueryVariables = Exact<{
  page: Scalars['Int'];
  limit: Scalars['Int'];
  advanced: GetTasks;
}>;


export type GetTasksQuery = { __typename?: 'Query', getTasks: { __typename?: 'GetTasksResult', totalPages: number, tasks?: Array<{ __typename?: 'Task', _id?: string | null, name: string, point: number, createdAt: number }> | null } };

export type LoginMutationVariables = Exact<{
  username: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = { __typename?: 'Mutation', login: { __typename?: 'Auth', token: string, user: { __typename?: 'User', role: Role, username: string } } };

export type DeleteUserMutationVariables = Exact<{
  _id: Scalars['ID'];
}>;


export type DeleteUserMutation = { __typename?: 'Mutation', deleteUser: boolean };

export type EditUserMutationVariables = Exact<{
  _id: Scalars['ID'];
  user: EditUserInput;
}>;


export type EditUserMutation = { __typename?: 'Mutation', editUser: boolean };

export type AddUserMutationVariables = Exact<{
  user: AddUserInput;
}>;


export type AddUserMutation = { __typename?: 'Mutation', addUser: boolean };

export type GetUsersQueryVariables = Exact<{
  page: Scalars['Int'];
  limit: Scalars['Int'];
}>;


export type GetUsersQuery = { __typename?: 'Query', getUsers: { __typename?: 'GetUsersResult', totalPages: number, totalItems: number, users?: Array<{ __typename?: 'User', _id?: string | null, role: Role, username: string }> | null } };

export type GetUserQueryVariables = Exact<{
  _id: Scalars['ID'];
}>;


export type GetUserQuery = { __typename?: 'Query', getUser: { __typename?: 'User', _id?: string | null, username: string, role: Role } };


export const DeleteAttendanceDocument = gql`
    mutation deleteAttendance($_id: ID!) {
  deleteAttendance(_id: $_id)
}
    `;
export const EditAttendanceDocument = gql`
    mutation editAttendance($_id: ID!, $attendance: EditAttendanceInput!) {
  editAttendance(_id: $_id, attendance: $attendance)
}
    `;
export const AddAttendanceDocument = gql`
    mutation addAttendance($attendance: AddAttendanceInput!) {
  addAttendance(attendance: $attendance)
}
    `;
export const GetAttendancesDocument = gql`
    query getAttendances($page: Int!, $limit: Int!, $user: ID!) {
  getAttendances(page: $page, limit: $limit, user: $user) {
    attendances {
      _id
      status
      date
      user {
        _id
        username
      }
    }
    totalPages
  }
}
    `;
export const GetTaskDocument = gql`
    query getTask($id: ID!) {
  getTask(_id: $id) {
    _id
    name
    point
    createdAt
  }
}
    `;
export const DeleteSurveyDocument = gql`
    mutation deleteSurvey($_id: ID!) {
  deleteSurvey(_id: $_id)
}
    `;
export const EditSurveyDocument = gql`
    mutation editSurvey($_id: ID!, $survey: EditSurveyInput!) {
  editSurvey(_id: $_id, survey: $survey)
}
    `;
export const AddSurveyDocument = gql`
    mutation addSurvey($survey: AddSurveyInput!) {
  addSurvey(survey: $survey)
}
    `;
export const GetSurveysDocument = gql`
    query getSurveys($page: Int!, $limit: Int!, $user: ID!) {
  getSurveys(page: $page, limit: $limit, user: $user) {
    surveys {
      _id
      point
      user {
        _id
        username
      }
      task {
        _id
        point
        name
        createdAt
      }
    }
    totalPages
  }
}
    `;
export const DeleteTaskDocument = gql`
    mutation deleteTask($_id: ID!) {
  deleteTask(_id: $_id)
}
    `;
export const EditTaskDocument = gql`
    mutation editTask($_id: ID!, $task: EditTaskInput!) {
  editTask(_id: $_id, task: $task)
}
    `;
export const AddTaskDocument = gql`
    mutation addTask($task: AddTaskInput!) {
  addTask(task: $task)
}
    `;
export const GetTasksDocument = gql`
    query getTasks($page: Int!, $limit: Int!, $advanced: GetTasks!) {
  getTasks(page: $page, limit: $limit, advanced: $advanced) {
    tasks {
      _id
      name
      point
      createdAt
    }
    totalPages
  }
}
    `;
export const LoginDocument = gql`
    mutation login($username: String!, $password: String!) {
  login(username: $username, password: $password) {
    token
    user {
      role
      username
    }
  }
}
    `;
export const DeleteUserDocument = gql`
    mutation deleteUser($_id: ID!) {
  deleteUser(_id: $_id)
}
    `;
export const EditUserDocument = gql`
    mutation editUser($_id: ID!, $user: EditUserInput!) {
  editUser(_id: $_id, user: $user)
}
    `;
export const AddUserDocument = gql`
    mutation addUser($user: AddUserInput!) {
  addUser(user: $user)
}
    `;
export const GetUsersDocument = gql`
    query getUsers($page: Int!, $limit: Int!) {
  getUsers(page: $page, limit: $limit) {
    users {
      _id
      role
      username
    }
    totalPages
    totalItems
  }
}
    `;
export const GetUserDocument = gql`
    query getUser($_id: ID!) {
  getUser(_id: $_id) {
    _id
    username
    role
  }
}
    `;

export type SdkFunctionWrapper = <T>(action: (requestHeaders?:Record<string, string>) => Promise<T>, operationName: string, operationType?: string) => Promise<T>;


const defaultWrapper: SdkFunctionWrapper = (action, _operationName, _operationType) => action();

export function getSdk(client: GraphQLClient, withWrapper: SdkFunctionWrapper = defaultWrapper) {
  return {
    deleteAttendance(variables: DeleteAttendanceMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<DeleteAttendanceMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<DeleteAttendanceMutation>(DeleteAttendanceDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'deleteAttendance', 'mutation');
    },
    editAttendance(variables: EditAttendanceMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<EditAttendanceMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<EditAttendanceMutation>(EditAttendanceDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'editAttendance', 'mutation');
    },
    addAttendance(variables: AddAttendanceMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AddAttendanceMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<AddAttendanceMutation>(AddAttendanceDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'addAttendance', 'mutation');
    },
    getAttendances(variables: GetAttendancesQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetAttendancesQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<GetAttendancesQuery>(GetAttendancesDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'getAttendances', 'query');
    },
    getTask(variables: GetTaskQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetTaskQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<GetTaskQuery>(GetTaskDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'getTask', 'query');
    },
    deleteSurvey(variables: DeleteSurveyMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<DeleteSurveyMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<DeleteSurveyMutation>(DeleteSurveyDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'deleteSurvey', 'mutation');
    },
    editSurvey(variables: EditSurveyMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<EditSurveyMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<EditSurveyMutation>(EditSurveyDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'editSurvey', 'mutation');
    },
    addSurvey(variables: AddSurveyMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AddSurveyMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<AddSurveyMutation>(AddSurveyDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'addSurvey', 'mutation');
    },
    getSurveys(variables: GetSurveysQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetSurveysQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<GetSurveysQuery>(GetSurveysDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'getSurveys', 'query');
    },
    deleteTask(variables: DeleteTaskMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<DeleteTaskMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<DeleteTaskMutation>(DeleteTaskDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'deleteTask', 'mutation');
    },
    editTask(variables: EditTaskMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<EditTaskMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<EditTaskMutation>(EditTaskDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'editTask', 'mutation');
    },
    addTask(variables: AddTaskMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AddTaskMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<AddTaskMutation>(AddTaskDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'addTask', 'mutation');
    },
    getTasks(variables: GetTasksQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetTasksQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<GetTasksQuery>(GetTasksDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'getTasks', 'query');
    },
    login(variables: LoginMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<LoginMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<LoginMutation>(LoginDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'login', 'mutation');
    },
    deleteUser(variables: DeleteUserMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<DeleteUserMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<DeleteUserMutation>(DeleteUserDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'deleteUser', 'mutation');
    },
    editUser(variables: EditUserMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<EditUserMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<EditUserMutation>(EditUserDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'editUser', 'mutation');
    },
    addUser(variables: AddUserMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AddUserMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<AddUserMutation>(AddUserDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'addUser', 'mutation');
    },
    getUsers(variables: GetUsersQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetUsersQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<GetUsersQuery>(GetUsersDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'getUsers', 'query');
    },
    getUser(variables: GetUserQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetUserQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<GetUserQuery>(GetUserDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'getUser', 'query');
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;