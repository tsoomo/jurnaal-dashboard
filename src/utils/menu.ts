import { UserOutlined } from "@ant-design/icons";

export const categories = [
  {
    key: "0",
    title: "Хяналтын самбар",
    path: "/dashboard",
    icon: "",
  },
  {
    key: "1",
    title: "Сурагчид",
    path: "/user",
    icon: "",
  },
  {
    key: "2",
    title: "Даалгавар",
    path: "/task",
    icon: "",
  },
];
