import jwt_decode from "jwt-decode";
import { Role } from "../graphql";

type User = {
  id: string;
  username: string;
  role: string;
};

export const isLoggedIn = () => {
  try {
    if (
      !sessionStorage.getItem("auth") ||
      sessionStorage.getItem("auth") === null
    )
      return false;
    return true;
  } catch {
    return false;
  }
};

export const getUserData = () => {
  var token = sessionStorage.getItem("auth");
  if (!token) return null;
  if (token?.split(" ")[0] !== "Bearer") return null;
  try {
    var decoded: any = jwt_decode(token);
    var userData: User = decoded.user;

    if (typeof userData !== "object") return null;
    if (userData.role === Role.Teacher) return null;
  } catch {
    return null;
  }

  return userData;
};
