import { useEffect } from "react";

export const NotfoundPage = () => {
  useEffect(() => {
    document.title = "404 not found";
  }, []);
  return (
    <div className="not_found">
      <img src="/404.png" alt="404" />
    </div>
  );
};
