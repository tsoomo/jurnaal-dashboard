import { useEffect } from "react";
import { UserWidget } from "../widgets/user";

const UserPage = () => {
  useEffect(() => {
    document.title = "Хэрэглэгчид";
  }, []);
  return (
    <div className="page">
      <UserWidget />
    </div>
  );
};
export default UserPage;
