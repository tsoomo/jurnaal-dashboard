import { useEffect } from "react";
import { AttendanceWidget } from "../widgets/attendance";

const AttendancePage = () => {
  useEffect(() => {
    document.title = "Ирц дэлгэрэнгүй";
  }, []);
  return (
    <div className="page">
      <AttendanceWidget />
    </div>
  );
};
export default AttendancePage;
