import { useEffect } from "react";
import { LoginWidget } from "../widgets/login";

export const LoginPage = () => {
  useEffect(() => {
    document.title = "Нэвтрэх";
  }, []);
  return (
    <div className="login">
      <div className="login-form">
        <div className="logo">
          <img src="/logo.png" alt="logo" />
        </div>
        <LoginWidget />
      </div>
    </div>
  );
};
