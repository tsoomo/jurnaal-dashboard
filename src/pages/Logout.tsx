import { useEffect } from "react";
import { useHistory } from "react-router";

export const LogoutPage = () => {
  const history = useHistory();

  useEffect(() => {
    sessionStorage.clear();
    history.push("/");
  }, [history]);
  return <div>Loading...</div>;
};
