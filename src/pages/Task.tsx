import { useEffect } from "react";
import { TaskWidget } from "../widgets/task";

const TaskPage = () => {
  useEffect(() => {
    document.title = "Даалгавар";
  }, []);
  return (
    <div className="page">
      <TaskWidget />
    </div>
  );
};
export default TaskPage;
