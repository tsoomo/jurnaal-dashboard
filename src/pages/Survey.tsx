import { useEffect } from "react";
import { SurveyWidget } from "../widgets/survey";

const SurveyPage = () => {
  useEffect(() => {
    document.title = "Даалгавар дэлгэрэнгүй";
  }, []);
  return (
    <div className="page">
      <SurveyWidget />
    </div>
  );
};
export default SurveyPage;
