import { createElement } from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Layout from "./components/Layout";
import TaskPage from "./pages/Task";
import { LoginPage } from "./pages/Login";
import { LogoutPage } from "./pages/Logout";
import { NotfoundPage } from "./pages/Notfound";
import UserPage from "./pages/User";
import { isLoggedIn } from "./utils/auth";
import AttendancePage from "./pages/Attendance";
import SurveyPage from "./pages/Survey";

const ProtectedRoute = ({ component, ...rest }: any) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        isLoggedIn() ? (
          <Layout children={createElement(component, props)} />
        ) : (
          <Redirect to="/logout" />
        )
      }
    />
  );
};

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route component={LoginPage} path="/" exact />
        <Route path="/logout" component={LogoutPage} />
        <ProtectedRoute exact component={UserPage} path="/user" />
        <ProtectedRoute exact component={AttendancePage} path="/user/:id" />
        <ProtectedRoute exact component={TaskPage} path="/task" />
        <ProtectedRoute exact component={SurveyPage} path="/task/:id" />
        <Route component={NotfoundPage} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
