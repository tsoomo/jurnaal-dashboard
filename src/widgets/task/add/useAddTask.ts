import { Form, message } from "antd";
import { Dispatch, useCallback, useState } from "react";
import { AddTaskInput } from "../../../graphql";
import sdk from "../../../sdk";

const useAddTask = ({
  getTasks,
  setVisible,
}: {
  getTasks: () => void;
  setVisible: Dispatch<boolean>;
}) => {
  const [form] = Form.useForm<AddTaskInput>();
  const [loading, setLoading] = useState(false);

  const addTask = useCallback(
    (values: AddTaskInput) => {
      setLoading(true);
      sdk
        .addTask({
          task: {
            createdAt: Date.now(),
            point: values.point,
            name: values.name,
          },
        })
        .then(() => {
          getTasks();
          message.success("Амжилттай нэмэгдлээ");
          form.resetFields();
          setVisible(false);
        })
        .catch((err) => message.error(err.message))
        .finally(() => setLoading(false));
    },
    [form, getTasks, setVisible]
  );

  return { loading, form, addTask };
};

export default useAddTask;
