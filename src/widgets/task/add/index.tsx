import { Button, Input, Form, Modal, Col, Row, InputNumber } from "antd";
import { Dispatch } from "react";
import useAddTask from "./useAddTask";

export const AddTaskWidget = ({
  visible,
  setVisible,
  getTasks,
}: {
  visible: boolean;
  setVisible: Dispatch<boolean>;
  getTasks: () => void;
}) => {
  const { loading, form, addTask } = useAddTask({ getTasks, setVisible });
  return (
    <Modal
      width={400}
      title="Даалгавар нэмэх"
      visible={visible}
      onCancel={() => setVisible(false)}
      footer={[
        <Button key="back" onClick={() => setVisible(false)} disabled={loading}>
          Болих
        </Button>,
        <Button
          key="submit"
          type="primary"
          loading={loading}
          onClick={() => form.validateFields().then(addTask)}
        >
          Нэмэх
        </Button>,
      ]}
    >
      <Form layout="vertical" onFinish={addTask} autoComplete="off" form={form}>
        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label="Даалгаврын нэр"
              name="name"
              rules={[{ required: true, message: "Даалгаврын нэр оруулна уу" }]}
            >
              <Input.TextArea disabled={loading} />
            </Form.Item>
            <Form.Item
              label="Даалгаврын оноо"
              name="point"
              rules={[
                { required: true, message: "Даалгаврын оноо оруулна уу" },
              ]}
            >
              <InputNumber disabled={loading} />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};
