import { Button, Input, Form, Modal, Row, Col } from "antd";
import { Dispatch, useEffect } from "react";
import { Task } from "../../../graphql";
import { useEditTask } from "./useEditTask";

export const EditTaskWidget = ({
  task,
  setTask,
  getTasks,
}: {
  task: Task | null;
  setTask: Dispatch<Task | null>;
  getTasks: () => void;
}) => {
  const { form, loading, editTask } = useEditTask({
    selectedTaskId: task?._id!,
    getTasks,
    setTask,
  });
  useEffect(() => {
    if (task) {
      form.setFields([
        {
          name: "name",
          value: task.name,
        },
        {
          name: "point",
          value: task.point,
        },
      ]);
    }
  }, [form, task]);
  return (
    <Modal
      width={400}
      title="Даалгавар өөрчлөх"
      visible={!!task}
      onCancel={() => setTask(null)}
      footer={[
        <Button key="back" disabled={loading} onClick={() => setTask(null)}>
          Болих
        </Button>,
        <Button
          key="submit"
          type="primary"
          disabled={loading}
          onClick={() => form.validateFields().then(editTask)}
        >
          Хадгалах
        </Button>,
      ]}
    >
      <Form
        form={form}
        layout="vertical"
        onFinish={editTask}
        autoComplete="off"
      >
        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label="Даалгаврын нэр"
              name="name"
              rules={[{ required: true }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Даалгаврын оноо"
              name="point"
              rules={[{ required: true }]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};
