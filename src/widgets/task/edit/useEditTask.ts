import { Form, message } from "antd";
import { Dispatch, useCallback, useState } from "react";
import { EditTaskInput, Task } from "../../../graphql";
import sdk from "../../../sdk";

export const useEditTask = ({
  selectedTaskId,
  getTasks,
  setTask,
}: {
  selectedTaskId?: string;
  getTasks: () => void;
  setTask: Dispatch<Task | null>;
}) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);

  const editTask = useCallback(
    (values: EditTaskInput) => {
      setLoading(true);
      if (selectedTaskId) {
        sdk
          .editTask({
            _id: selectedTaskId,
            task: {
              name: values.name,
              point: values.point,
            },
          })
          .then(() => {
            setTask(null);
            getTasks();
            message.success("Амжилттай засагдлаа", 2);
            setLoading(false);
          })
          .catch((err) => {
            message.error(err.message, 2);
            setLoading(false);
          })
          .finally(() => setLoading(false));
      }
    },
    [getTasks, selectedTaskId, setTask]
  );
  return {
    form,
    loading,
    editTask,
  };
};
