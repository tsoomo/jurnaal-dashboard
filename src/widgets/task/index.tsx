import {
  Button,
  Col,
  DatePicker,
  Input,
  Popconfirm,
  Row,
  Select,
  Space,
  Table,
  Tag,
  Tooltip,
  Typography,
} from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  EyeOutlined,
} from "@ant-design/icons";
import { useState } from "react";
import { useHistory } from "react-router-dom";

import { useTasks } from "./useTasks";
import { AddTaskWidget } from "./add";
import { EditTaskWidget } from "./edit";
import moment from "moment";

const { Text, Title } = Typography;
const { RangePicker } = DatePicker;
const { Search } = Input;
const { Option } = Select;

export const TaskWidget = () => {
  const history = useHistory();
  const [task, setTask] = useState<any | null>(null);
  const { data, loading, getTasks, setPage, deleteTask } = useTasks();
  const [addTask, setAddTask] = useState(false);

  return (
    <>
      <AddTaskWidget
        visible={addTask}
        setVisible={setAddTask}
        getTasks={getTasks}
      />
      <EditTaskWidget task={task} setTask={setTask} getTasks={getTasks} />
      <div className="page-header">
        <h3>Даалгавар</h3>
        <Space>
          <Button
            type="primary"
            onClick={() => setAddTask(true)}
            icon={<PlusOutlined />}
          >
            Нэмэх
          </Button>
        </Space>
      </div>
      <div className="page-body">
        <div className="page-table">
          <Row gutter={[30, 30]} justify="end">
            <Col span={24}>
              <Table
                scroll={{ x: 900 }}
                size="small"
                loading={loading}
                dataSource={data?.tasks ?? []}
                pagination={{
                  pageSize: 10,
                  total: Number(data?.totalPages) * 10,
                }}
                bordered
                columns={[
                  {
                    title: "Өдөр",
                    dataIndex: "createdAt",
                    key: "createdAt",
                    render: (text) => (
                      <span className="text_ellipse">
                        {moment(new Date(text).toString()).format("YYYY/MM/DD")}
                      </span>
                    ),
                  },
                  {
                    title: "Даалгаврын нэр",
                    dataIndex: "name",
                    key: "name",
                    render: (text) => (
                      <span className="text_ellipse">{text}</span>
                    ),
                  },
                  {
                    title: "Авах оноо",
                    dataIndex: "point",
                    key: "point",
                    render: (text) => (
                      <span className="text_ellipse">{text}</span>
                    ),
                  },
                  {
                    title: "Үйлдлүүд",
                    dataIndex: "button",
                    key: "button",
                    fixed: "right",
                    width: "90px",
                    render: (_, record) => {
                      return (
                        <Space>
                          <Button
                            type="primary"
                            shape="circle"
                            disabled={loading}
                            onClick={() => {
                              if (record) {
                                setTask(record);
                              }
                            }}
                            icon={<EditOutlined />}
                          />
                          <Popconfirm
                            disabled={loading}
                            title="Та устгахдаа итгэлтэй байна уу?"
                            okText="Тийм"
                            cancelText="Үгүй"
                            onConfirm={() => {
                              if (record._id) {
                                deleteTask(record._id);
                              }
                            }}
                          >
                            <Button
                              size="middle"
                              shape="circle"
                              disabled={loading}
                              danger
                              type="primary"
                              icon={<DeleteOutlined />}
                            />
                          </Popconfirm>
                        </Space>
                      );
                    },
                  },
                ]}
              />
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
};
