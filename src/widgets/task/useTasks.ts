import { message } from "antd";
import { useCallback, useEffect, useState } from "react";
import { GetTasksQuery } from "../../graphql";
import sdk from "../../sdk";

export const useTasks = () => {
  const [data, setData] = useState<GetTasksQuery["getTasks"]>();
  const [page, setPage] = useState(0);
  const [allTasks, setAllTasks] = useState<GetTasksQuery["getTasks"]>();
  const [loading, setLoading] = useState(false);
  const getTasks = useCallback(() => {
    setLoading(true);
    sdk
      .getTasks({ limit: 15, page, advanced: {} })
      .then((data) => setData(data.getTasks))
      .catch((err) => {
        message.error(err.message, 2);
        setLoading(false);
      })
      .finally(() => setLoading(false));
  }, [page]);

  const deleteTask = (id: string) => {
    sdk
      .deleteTask({ _id: id })
      .then(() => {
        message.success("Амжилттай устгагдлаа");
        getTasks();
      })
      .catch((err) => {
        message.error(err.message, 2);
        setLoading(false);
      })
      .finally(() => setLoading(false));
  };

  useEffect(() => {
    getTasks();
  }, [getTasks]);
  return {
    data,
    loading,
    getTasks,
    setPage,
    deleteTask,
  };
};
