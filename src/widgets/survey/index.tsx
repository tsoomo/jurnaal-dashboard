import {
  Table,
  Button,
  Row,
  Col,
  Spin,
  Space,
  DatePicker,
  Tag,
  Tooltip,
  Popconfirm,
} from "antd";
import {
  ArrowLeftOutlined,
  PlusOutlined,
  EditOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import moment from "moment";
import { useHistory, useParams } from "react-router";

import { useSurvey } from "./useSurvey";
import useUserInfo from "../../hooks/useUserInfo";

export const SurveyWidget = () => {
  const { id }: { id: string } = useParams();
  const history = useHistory();
  const { RangePicker } = DatePicker;
  const { data, loading, setPage, getSurveys } = useSurvey({
    userId: id,
  });
  const { data: userData } = useUserInfo({ userId: id });
  return (
    <>
      <div className="page-header">
        <h3>Даалгавар дэлгэрэнгүй</h3>
        <Space>
          <Button
            icon={<ArrowLeftOutlined />}
            type="dashed"
            onClick={() => history.goBack()}
          >
            Буцах
          </Button>
        </Space>
      </div>
      <div className="page-body">
        <div className="page-table">
          <Row gutter={[24, 24]}>
            {loading && (
              <Col span={24} style={{ textAlign: "center" }}>
                <Spin />
              </Col>
            )}
            <Col span={6}>
              <p className="label_text">Сурагчийн нэр</p>
              {userData?.username}
            </Col>
            <Col span={24}>
              <Table
                scroll={{ x: 800 }}
                size="small"
                bordered
                loading={loading}
                dataSource={data?.surveys ?? []}
                pagination={{
                  pageSize: 20,
                  total: Number(data?.totalPages) * 20,
                }}
                onChange={(pagination) =>
                  setPage(Number(pagination?.current) - 1)
                }
                columns={[
                  {
                    title: "Огноо",
                    dataIndex: "createdAt",
                    key: "createdAt",
                    render: (text, record) => (
                      <span className="text_ellipse">
                        {moment(
                          new Date(record.task.createdAt).toString()
                        ).format("YYYY/MM/DD")}
                      </span>
                    ),
                  },
                  {
                    title: "Даалгаврын нэр",
                    dataIndex: "name",
                    key: "name",
                    render: (text, record) => (
                      <span className="text_ellipse">{record.task.name}</span>
                    ),
                  },
                  {
                    title: "Авах ёстой оноо",
                    dataIndex: "maxPoint",
                    key: "maxPoint",
                    render: (text, record) => (
                      <span className="text_ellipse">{record.task.point}</span>
                    ),
                  },
                  {
                    title: "Авсан оноо",
                    dataIndex: "point",
                    key: "point",
                    render: (text) => (
                      <span className="text_ellipse">{text}</span>
                    ),
                  },
                ]}
              />
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
};
