import { message } from "antd";
import moment from "moment";
import { useCallback, useState, useEffect } from "react";
import { DateRange, GetSurveysQuery } from "../../graphql";
import sdk from "../../sdk";

export const useSurvey = ({ userId }: { userId: string }) => {
  const [data, setData] = useState<GetSurveysQuery["getSurveys"]>();
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(0);
  const getSurveys = useCallback(() => {
    setLoading(true);
    sdk
      .getSurveys({
        limit: 20,
        page,
        user: userId,
      })
      .then((data) => setData(data.getSurveys))
      .catch((err) => {
        message.error(err.message, 2);
        setLoading(false);
      })
      .finally(() => setLoading(false));
  }, [page]);

  useEffect(() => {
    getSurveys();
  }, [getSurveys, page, userId]);

  return {
    data,
    loading,
    getSurveys,
    setPage,
  };
};
