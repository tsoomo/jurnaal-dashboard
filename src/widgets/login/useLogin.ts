import { useCallback, useState } from "react";
import { useHistory } from "react-router";
import { message } from "antd";
import sdk, { client } from "../../sdk";
import { LoginMutationVariables } from "../../graphql";

const useLogin = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const submit = useCallback(
    (values: LoginMutationVariables) => {
      setLoading(true);
      sdk
        .login({ username: values.username, password: values.password })
        .then(({ login }) => {
          sessionStorage.setItem("auth", `Bearer ${login.token}`);
          client.setHeader("Authorization", `Bearer ${login.token}`);
          history.push("/user");
        })
        .catch((err) => {
          message.error(err.message, 2);
          setLoading(false);
        })
        .finally(() => setLoading(false));
    },
    [history]
  );
  return {
    loading,
    submit,
  };
};

export default useLogin;
