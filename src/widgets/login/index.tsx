import { Form, Input, Button } from "antd";
import { LoginOutlined } from "@ant-design/icons";
import useLogin from "./useLogin";

export const LoginWidget = () => {
  const { loading, submit } = useLogin();
  return (
    <Form layout="vertical" onFinish={submit} autoComplete="off">
      <Form.Item
        label="Нэвтрэх нэр"
        name="username"
        style={{ marginBottom: "16px" }}
        rules={[{ required: true, message: "Нэвтрэх нэр оруулна уу!" }]}
      >
        <Input disabled={loading} autoComplete="new-password" />
      </Form.Item>
      <Form.Item
        label="Нууц үг"
        name="password"
        rules={[{ required: true, message: "Нууц үгээ оруулна уу!" }]}
      >
        <Input.Password disabled={loading} autoComplete="new-password" />
      </Form.Item>
      <Form.Item>
        <Button
          icon={<LoginOutlined />}
          style={{ width: "100%" }}
          htmlType="submit"
          type="primary"
          loading={loading}
        >
          Нэвтрэх
        </Button>
      </Form.Item>
    </Form>
  );
};
