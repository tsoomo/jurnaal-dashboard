import { Form, message } from "antd";
import { Dispatch, useCallback, useState } from "react";
import { AddAttendanceInput, Status } from "../../../graphql";
import sdk from "../../../sdk";

const useAddAttendance = ({
  selectedUserId,
  setId,
}: {
  selectedUserId?: string;
  setId: Dispatch<string | null>;
}) => {
  const [form] = Form.useForm<AddAttendanceInput>();
  const [loading, setLoading] = useState(false);

  const addAttendance = useCallback(
    (values: AddAttendanceInput) => {
      setLoading(true);
      if (selectedUserId) {
        sdk
          .addAttendance({
            attendance: {
              user: selectedUserId!,
              status: values.status,
              date: new Date(values.date).getTime(),
            },
          })
          .then(() => {
            message.success("Амжилттай бүртгэгдлээ");
            form.resetFields();
            setId(null);
          })
          .catch((err) => message.error(err.message))
          .finally(() => setLoading(false));
      }
    },
    [form, setId, selectedUserId]
  );

  return { loading, form, addAttendance };
};

export default useAddAttendance;
