import {
  Button,
  Input,
  Form,
  Modal,
  Col,
  Row,
  Typography,
  DatePicker,
  Select,
} from "antd";
import moment from "moment";
import { Dispatch, useEffect } from "react";
import { Status, User } from "../../../graphql";
import useAddAttendance from "./useAttendance";
const { Option } = Select;
export const AddAttendanceWidget = ({
  userId,
  setId,
}: {
  setId: Dispatch<string | null>;
  userId: string | null;
}) => {
  const { loading, form, addAttendance } = useAddAttendance({
    selectedUserId: userId!,
    setId,
  });

  return (
    <Modal
      width={400}
      title="Ирц бүртгэх"
      visible={!!userId}
      onCancel={() => setId(null)}
      footer={[
        <Button key="back" onClick={() => setId(null)} disabled={loading}>
          Болих
        </Button>,
        <Button
          key="submit"
          type="primary"
          loading={loading}
          onClick={() => form.validateFields().then(addAttendance)}
        >
          Нэмэх
        </Button>,
      ]}
    >
      <Form
        layout="vertical"
        onFinish={addAttendance}
        autoComplete="off"
        form={form}
      >
        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label="Огноо"
              name="date"
              rules={[{ required: true, message: "Огноо оруулна уу" }]}
            >
              <DatePicker
                style={{ width: "100%" }}
                defaultValue={moment(Date.now())}
                disabled={loading}
              />
            </Form.Item>
            <Form.Item
              label="Ирсэн эсэх"
              name="status"
              rules={[{ required: true, message: "Огноо оруулна уу" }]}
            >
              <Select style={{ width: "100%" }} disabled={loading}>
                <Option key={Status.Present} value={Status.Present}>
                  ИРСЭН
                </Option>
                <Option key={Status.Sick} value={Status.Sick}>
                  ӨВЧТЭЙ
                </Option>
                <Option key={Status.Free} value={Status.Free}>
                  ЧӨЛӨӨТЭЙ
                </Option>
                <Option key={Status.Absent} value={Status.Absent}>
                  ТАСАЛСАН
                </Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};
