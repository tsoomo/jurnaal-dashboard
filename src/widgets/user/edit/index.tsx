import { Button, Input, Form, Modal, Row, Col } from "antd";
import { Dispatch, useEffect } from "react";
import { User } from "../../../graphql";
import { useEditUser } from "./useEditUser";

export const EditUserWidget = ({
  user,
  setUser,
  getUsers,
}: {
  user: User | null;
  setUser: Dispatch<User | null>;
  getUsers: () => void;
}) => {
  const { form, loading, editUser } = useEditUser({
    selectedUserId: user?._id!,
    getUsers,
    setUser,
  });
  useEffect(() => {
    if (user) {
      form.setFields([
        {
          name: "username",
          value: user.username,
        },
      ]);
    }
  }, [form, user]);
  return (
    <Modal
      width={400}
      title="Сурагчийн мэдээлэл өөрчлөх"
      visible={!!user}
      onCancel={() => setUser(null)}
      footer={[
        <Button key="back" disabled={loading} onClick={() => setUser(null)}>
          Болих
        </Button>,
        <Button
          key="submit"
          type="primary"
          disabled={loading}
          onClick={() => form.validateFields().then(editUser)}
        >
          Хадгалах
        </Button>,
      ]}
    >
      <Form
        form={form}
        layout="vertical"
        onFinish={editUser}
        autoComplete="off"
      >
        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label="Нэвтрэх нэр"
              name="username"
              rules={[{ required: true }]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};
