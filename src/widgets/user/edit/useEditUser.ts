import { Form, message } from "antd";
import { Dispatch, useCallback, useState } from "react";
import { EditUserInput, User } from "../../../graphql";
import sdk from "../../../sdk";

export const useEditUser = ({
  selectedUserId,
  getUsers,
  setUser,
}: {
  selectedUserId?: string;
  getUsers: () => void;
  setUser: Dispatch<User | null>;
}) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);

  const editUser = useCallback(
    (values: EditUserInput) => {
      setLoading(true);
      if (selectedUserId) {
        sdk
          .editUser({
            _id: selectedUserId,
            user: {
              username: values.username,
              password: values.password,
            },
          })
          .then(() => {
            setUser(null);
            getUsers();
            message.success("Амжилттай засагдлаа", 2);
            setLoading(false);
          })
          .catch((err) => {
            message.error(err.message, 2);
            setLoading(false);
          })
          .finally(() => setLoading(false));
      }
    },
    [getUsers, selectedUserId, setUser]
  );
  return {
    form,
    loading,
    editUser,
  };
};
