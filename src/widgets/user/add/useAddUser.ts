import { Form, message } from "antd";
import { Dispatch, useCallback, useState } from "react";
import { AddUserInput, Role } from "../../../graphql";
import sdk from "../../../sdk";

const useAddUser = ({
  getUsers,
  setVisible,
}: {
  getUsers: () => void;
  setVisible: Dispatch<boolean>;
}) => {
  const [form] = Form.useForm<AddUserInput>();
  const [loading, setLoading] = useState(false);

  const addUser = useCallback(
    (values: AddUserInput) => {
      setLoading(true);
      sdk
        .addUser({
          user: {
            username: values.username,
            password: values.username,
            role: Role.Student,
          },
        })
        .then(() => {
          getUsers();
          message.success("Амжилттай нэмэгдлээ");
          form.resetFields();
          setVisible(false);
        })
        .catch((err) => message.error(err.message))
        .finally(() => setLoading(false));
    },
    [form, getUsers, setVisible]
  );

  return { loading, form, addUser };
};

export default useAddUser;
