import { Button, Input, Form, Modal, Col, Row } from "antd";
import { Dispatch } from "react";
import useAddUser from "./useAddUser";

export const AddUserWidget = ({
  visible,
  setVisible,
  getUsers,
}: {
  visible: boolean;
  setVisible: Dispatch<boolean>;
  getUsers: () => void;
}) => {
  const { loading, form, addUser } = useAddUser({ getUsers, setVisible });
  return (
    <Modal
      width={400}
      title="Сурагч нэмэх"
      visible={visible}
      onCancel={() => setVisible(false)}
      footer={[
        <Button key="back" onClick={() => setVisible(false)} disabled={loading}>
          Болих
        </Button>,
        <Button
          key="submit"
          type="primary"
          loading={loading}
          onClick={() => form.validateFields().then(addUser)}
        >
          Нэмэх
        </Button>,
      ]}
    >
      <Form layout="vertical" onFinish={addUser} autoComplete="off" form={form}>
        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label="Сурагчийн нэр"
              name="username"
              rules={[{ required: true, message: "Сурагчийн нэр оруулна уу" }]}
            >
              <Input disabled={loading} />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};
