import { message } from "antd";
import { useCallback, useEffect, useState } from "react";
import { GetUsersQuery } from "../../graphql";
import sdk from "../../sdk";

export const useUsers = () => {
  const [data, setData] = useState<GetUsersQuery>();
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(0);
  const getUsers = useCallback(() => {
    setLoading(true);
    sdk
      .getUsers({ limit: 10, page })
      .then((data) => setData(data))
      .catch((err) => {
        message.error(err.message, 2);
        setLoading(false);
      })
      .finally(() => setLoading(false));
  }, [page]);
  const deleteUser = (id: string) => {
    sdk
      .deleteUser({ _id: id })
      .then(() => {
        message.success("Амжилттай устгагдлаа");
        getUsers();
      })
      .catch((err) => {
        message.error(err.message, 2);
        setLoading(false);
      })
      .finally(() => setLoading(false));
  };

  useEffect(() => {
    getUsers();
  }, [getUsers]);
  return {
    data: data?.getUsers,
    loading,
    getUsers,
    deleteUser,
    setPage,
  };
};
