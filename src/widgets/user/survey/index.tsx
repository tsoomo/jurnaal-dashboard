import {
  Button,
  Input,
  Form,
  Modal,
  Col,
  Row,
  Typography,
  DatePicker,
  Select,
  InputNumber,
} from "antd";
import moment from "moment";
import { Dispatch, useEffect } from "react";
import { Status, User } from "../../../graphql";
import useTasks from "../../../hooks/useTask";
import useAddSurvey from "./useSurvey";
const { Option } = Select;
export const AddSurveyWidget = ({
  userId,
  setId,
}: {
  setId: Dispatch<string | null>;
  userId: string | null;
}) => {
  const { loading, form, addSurvey } = useAddSurvey({
    selectedUserId: userId!,
    setId,
  });
  const { data } = useTasks();
  return (
    <Modal
      width={600}
      title="Ирц бүртгэх"
      visible={!!userId}
      onCancel={() => setId(null)}
      footer={[
        <Button key="back" onClick={() => setId(null)} disabled={loading}>
          Болих
        </Button>,
        <Button
          key="submit"
          type="primary"
          loading={loading}
          onClick={() => form.validateFields().then(addSurvey)}
        >
          Нэмэх
        </Button>,
      ]}
    >
      <Form
        layout="vertical"
        onFinish={addSurvey}
        autoComplete="off"
        form={form}
      >
        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label="Даалгавар"
              name="task"
              rules={[{ required: true, message: "Даалгавар сонгоно уу" }]}
            >
              <Select style={{ width: "100%" }}>
                {data?.tasks!.map((task) => (
                  <Option key={task._id} value={task._id}>
                    {task.name} || АВАХ ОНОО {task.point}
                  </Option>
                ))}
              </Select>
            </Form.Item>

            <Form.Item
              label="Авсан оноо"
              name="point"
              rules={[
                { required: true, message: "Сурагчийн авсан оноо оруулна уу" },
              ]}
            >
              <InputNumber disabled={loading} />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};
