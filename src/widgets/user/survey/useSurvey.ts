import { Form, message } from "antd";
import { Dispatch, useCallback, useState } from "react";
import { AddAttendanceInput, AddSurveyInput, Status } from "../../../graphql";
import sdk from "../../../sdk";

const useAddSurvey = ({
  selectedUserId,
  setId,
}: {
  selectedUserId?: string;
  setId: Dispatch<string | null>;
}) => {
  const [form] = Form.useForm<AddSurveyInput>();
  const [loading, setLoading] = useState(false);
  const addSurvey = useCallback(
    (values: AddSurveyInput) => {
      setLoading(true);
      if (selectedUserId) {
        sdk
          .addSurvey({
            survey: {
              user: selectedUserId!,
              task: values.task,
              point: values.point,
            },
          })
          .then(() => {
            message.success("Амжилттай бүртгэгдлээ");
            form.resetFields();
            setId(null);
          })
          .catch((err) => message.error(err.message))
          .finally(() => setLoading(false));
      }
    },
    [form, setId, selectedUserId]
  );

  return { loading, form, addSurvey };
};

export default useAddSurvey;
