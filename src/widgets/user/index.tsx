import { Button, Col, Popconfirm, Row, Space, Table, Tooltip } from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  EyeOutlined,
  CalendarOutlined,
  FileDoneOutlined,
  CarryOutOutlined,
  ReconciliationOutlined,
} from "@ant-design/icons";
import { useState } from "react";
import { useHistory } from "react-router-dom";

import { useUsers } from "./useUsers";
import { EditUserWidget } from "./edit";
import { AddUserWidget } from "./add";
import { AddAttendanceWidget } from "./attendance";
import { AddSurveyWidget } from "./survey";

export const UserWidget = () => {
  const history = useHistory();
  const [user, setUser] = useState<any | null>(null);
  const { data, loading, deleteUser, getUsers, setPage } = useUsers();
  const [addUser, setAddUser] = useState(false);
  const [userId, setId] = useState<string | null>(null);
  const [survey, setSurvey] = useState<string | null>(null);
  return (
    <>
      <AddUserWidget
        visible={addUser}
        setVisible={setAddUser}
        getUsers={getUsers}
      />
      <EditUserWidget user={user} setUser={setUser} getUsers={getUsers} />
      <AddAttendanceWidget userId={userId!} setId={setId} />
      <AddSurveyWidget userId={survey!} setId={setSurvey} />
      <div className="page-header">
        <h3>Хэрэглэгчид</h3>
        <Space>
          <Button
            type="primary"
            onClick={() => setAddUser(true)}
            icon={<PlusOutlined />}
          >
            Нэмэх
          </Button>
        </Space>
      </div>
      <div className="page-body">
        <Row gutter={[30, 30]} justify="end">
          <Col span={24}>
            <div className="page-table">
              <Table
                scroll={{ x: 900 }}
                size="small"
                loading={loading}
                dataSource={data?.users ?? []}
                pagination={{
                  pageSize: 10,
                  total: Number(data?.totalPages) * 10,
                }}
                onChange={(pagination) =>
                  setPage(Number(pagination?.current) - 1)
                }
                bordered
                columns={[
                  {
                    title: "Сурагчийн нэр",
                    dataIndex: "username",
                    key: "_id",
                    render: (text) => (
                      <span className="text_ellipse">{text}</span>
                    ),
                  },
                  {
                    title: "Үйлдлүүд",
                    dataIndex: "button",
                    key: "button",
                    fixed: "right",
                    width: "265px",
                    render: (_, record) => {
                      return (
                        <Space>
                          <Tooltip title="Ирц дэлгэрэнгүй">
                            <Button
                              shape="circle"
                              disabled={loading}
                              icon={<CalendarOutlined />}
                              onClick={() =>
                                history.push(`/user/${record._id}`)
                              }
                            />
                          </Tooltip>
                          <Tooltip title="Ирц бүртгэх">
                            <Button
                              type="primary"
                              shape="circle"
                              disabled={loading}
                              onClick={() => {
                                if (record) {
                                  setId(record._id!);
                                }
                              }}
                              icon={<CarryOutOutlined />}
                            />
                          </Tooltip>
                          <Tooltip title="Даалгавар дэлгэрэнгүй">
                            <Button
                              shape="circle"
                              disabled={loading}
                              icon={<ReconciliationOutlined />}
                              onClick={() =>
                                history.push(`/task/${record._id}`)
                              }
                            />
                          </Tooltip>
                          <Tooltip title="Даалгавар шалгах">
                            <Button
                              type="primary"
                              shape="circle"
                              disabled={loading}
                              onClick={() => {
                                if (record) {
                                  setSurvey(record._id!);
                                }
                              }}
                              icon={<FileDoneOutlined />}
                            />
                          </Tooltip>
                          <Tooltip title="Сурагчийн нэр өөрчлөх">
                            <Button
                              type="primary"
                              shape="circle"
                              disabled={loading}
                              onClick={() => {
                                if (record) {
                                  setUser(record);
                                }
                              }}
                              icon={<EditOutlined />}
                            />
                          </Tooltip>
                          <Tooltip title="Сурагч хасах">
                            <Popconfirm
                              disabled={loading}
                              title="Та устгахдаа итгэлтэй байна уу?"
                              okText="Тийм"
                              cancelText="Үгүй"
                              onConfirm={() => {
                                if (record._id) {
                                  deleteUser(record._id);
                                }
                              }}
                            >
                              <Button
                                size="middle"
                                shape="circle"
                                disabled={loading}
                                danger
                                type="primary"
                                icon={<DeleteOutlined />}
                              />
                            </Popconfirm>
                          </Tooltip>
                        </Space>
                      );
                    },
                  },
                ]}
              />
            </div>
          </Col>
        </Row>
      </div>
    </>
  );
};
