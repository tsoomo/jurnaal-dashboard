import { message } from "antd";
import moment from "moment";
import { useCallback, useState, useEffect } from "react";
import { DateRange, GetAttendancesQuery } from "../../graphql";
import sdk from "../../sdk";

export const useAttendance = ({ userId }: { userId: string }) => {
  const [data, setData] = useState<GetAttendancesQuery["getAttendances"]>();
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(0);
  const getAttendances = useCallback(() => {
    setLoading(true);
    sdk
      .getAttendances({
        limit: 20,
        page,
        user: userId,
      })
      .then((data) => setData(data.getAttendances))
      .catch((err) => {
        message.error(err.message, 2);
        setLoading(false);
      })
      .finally(() => setLoading(false));
  }, [page]);

  useEffect(() => {
    getAttendances();
  }, [getAttendances, page, userId]);

  return {
    data,
    loading,
    getAttendances,
    setPage,
  };
};
