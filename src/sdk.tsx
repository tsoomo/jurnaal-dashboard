import { GraphQLClient } from "graphql-request";

import { getSdk, SdkFunctionWrapper } from "./graphql";

export interface GraphQLError extends Error {
  response: {
    errors: {
      message: string;
    }[];
  };
}

const callAndHandleError: SdkFunctionWrapper = (action) =>
  action().catch((err) => {
    const error = err.response.errors[0];
    throw new Error(error?.message);
  });

export const client = new GraphQLClient(
  process.env.NODE_ENV === "production"
    ? "/graphql"
    : "http://localhost:4000/graphql",
  {
    headers: {
      Authorization: `${sessionStorage.getItem("auth") || ""}`,
    },
  }
);

const sdk = getSdk(client, callAndHandleError);

export default sdk;
