import {
  CalendarOutlined,
  FileDoneOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Menu } from "antd";
import { useMemo } from "react";
import { Link, useHistory } from "react-router-dom";
import { categories } from "../utils/menu";

export const Sidebar = ({ collapsed }: { collapsed: boolean }) => {
  const history = useHistory();

  const selectedMenu = useMemo(
    () =>
      categories
        .filter(
          (category) => category.path === history.location.pathname.toString()
        )[0]
        ?.key.toString(),
    [history.location.pathname]
  );

  return (
    <Menu
      style={{
        position: "relative",
        height: "100vh",

        width: collapsed ? 70 : 230,
      }}
      selectedKeys={[selectedMenu]}
      mode="inline"
      inlineCollapsed={collapsed}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: 70,
          borderBottom: "1px solid #f0f0f0",
          marginBottom: 24,
        }}
      >
        <img
          style={{
            width: collapsed ? 55 : 100,
            transition: "0.3s",
          }}
          src="/logo.png"
          alt="logo"
        />
      </div>
      <Menu.Item
        eventKey="1"
        key={0}
        icon={<UserOutlined />}
        title={"Сурагчид"}
      >
        <Link to="/user">Сурагчид</Link>
      </Menu.Item>
      <Menu.Item
        eventKey="2"
        key={1}
        icon={<FileDoneOutlined />}
        title={"Даалгавар"}
      >
        <Link to="/task">Даалгавар</Link>
      </Menu.Item>
    </Menu>
  );
};
