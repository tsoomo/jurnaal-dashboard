import { Avatar, Button, Space, Tooltip } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  QuestionOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Dispatch } from "react";
import { useHistory } from "react-router-dom";

export const Header = ({
  collapsed,
  setCollapsed,
}: {
  collapsed: boolean;
  setCollapsed: Dispatch<boolean>;
}) => {
  const history = useHistory();
  return (
    <div className="header">
      <div
        onClick={() => setCollapsed(!collapsed)}
        style={{ cursor: "pointer", fontSize: 18 }}
      >
        {collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
      </div>
      <Space>
        <Avatar
          size="large"
          style={{ color: "#ffa600", backgroundColor: "#fde3cf" }}
        >
          U
        </Avatar>
        <Tooltip title="Туслах">
          <Button
            icon={<QuestionOutlined />}
            shape="circle"
            danger
            type="primary"
            size="large"
          />
        </Tooltip>
        <Tooltip title="Системээс гарах">
          <Button
            onClick={() => history.push("/logout")}
            icon={<LogoutOutlined />}
            shape="circle"
            type="primary"
            size="large"
          />
        </Tooltip>
      </Space>
    </div>
  );
};
