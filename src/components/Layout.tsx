import React, { ReactNode, useState } from "react";
import { Header } from "./Header";
import { Sidebar } from "./Sidebar";

const Layout = ({ children }: { children: ReactNode }) => {
  const [collapsed, setCollapsed] = useState(false);
  return (
    <div className="container">
      <Sidebar collapsed={collapsed} />
      <div style={{ width: "100%", background: "transparent" }}>
        <Header collapsed={collapsed} setCollapsed={setCollapsed} />
        <div className="container-body">{children}</div>
      </div>
    </div>
  );
};

export default Layout;
